let work = new Work();
let bank = new Bank();

let useCustomImages = false;

/* work and bank */
let displayPay = document.getElementById("pay-value");
let displayBalance = document.getElementById("balance-value");
let displayLoan = document.getElementById("loan-value");
updateWorkDisplay();
let workButton = document.getElementById("work-button");
let bankButton = document.getElementById("bank-button");
let loanButton = document.getElementById("loan-button");
let buyNowButton = document.getElementById("buy-now-button");
let payLoanButton = document.getElementById("pay-loan-button");
let customImageButton = document.getElementById("custom-img-button");
let loanValueContainer = document.getElementById("loan-value-container");

/* laptop */
let laptopSelect = document.getElementById("laptop-select");
let laptopFeatureList = document.getElementById("laptop-features-list");
let laptopImage = document.getElementById("laptop-image");
let laptopTitle = document.getElementById("laptop-title");
let laptopDescription = document.getElementById("laptop-description");
let laptopPrice = document.getElementById("laptop-price");
let laptopStock = document.getElementById("laptop-stock");

let computers = [];
const processComputersData = (computers) => {
    computers.forEach(computer => {
        //create option
        let computerOption = document.createElement("option");
        computerOption.value = computer.id;
        computerOption.innerText = computer.title;
        laptopSelect.appendChild(computerOption);
    });
}

function updateComputerInfo(id) {
    laptopFeatureList.innerHTML = '';
    if (useCustomImages) {
        laptopImage.src = computers[id].image;
    } else {
        laptopImage.src = "https://noroff-komputer-store-api.herokuapp.com/" + computers[id].image;
    }
    laptopImage.alt = computers[id].title;
    laptopImage.onerror = () => {
        laptopImage.src = computers[id].image; //on error, use local replacement
    }
    laptopTitle.innerText = computers[id].title;
    laptopDescription.textContent = computers[id].description;
    laptopPrice.innerText = computers[id].price + " NOK";
    laptopStock.innerText = computers[id].stock + " available";
    computers[id].specs.forEach(spec => {
        let computerSpec = document.createElement("li");
        computerSpec.innerText = spec;
        laptopFeatureList.appendChild(computerSpec);
    });
    updateDisplays();
}

function updateBankDisplay() {
    displayBalance.innerText = bank.balance + "kr";
    displayLoan.innerText = bank.loan + "kr";
    if (bank.hasLoan) {
        loanValueContainer.style.visibility = "visible";
    } else {
        loanValueContainer.style.visibility = "hidden";
    }
}

function updateWorkDisplay() {
    displayPay.innerText = work.pay + "kr";
    if (bank.loan > 0) {
        payLoanButton.style.visibility = "visible";
    }
}

function updateLaptopDisplay() {
    if (bank.balance >= computers[laptopSelect.value - 1].price) {
        buyNowButton.disabled = false;
        buyNowButton.innerHTML = "Buy Now"
    } else {
        buyNowButton.disabled = true;
        buyNowButton.innerHTML = "insufficient funds"
    }
}

function updateDisplays() {
    updateBankDisplay();
    updateWorkDisplay();
    updateLaptopDisplay();
}

laptopSelect.addEventListener("change", (ev) => {
    console.log(computers[laptopSelect.value - 1].title);
    updateComputerInfo(laptopSelect.value - 1);
})

workButton.addEventListener("click", (() => {
    work.doWork();
    updateDisplays();
}));

bankButton.addEventListener("click", (() => {
    work.bankPay(bank);
    updateDisplays();
}));

loanButton.addEventListener("click", (() => {
    bank.getLoan(prompt("how much do you want to loan?"));
    updateDisplays();
}));

payLoanButton.addEventListener("click", (() => {
    bank.payLoan(work.pay);
    work.pay = 0;
    updateDisplays();
}))
customImageButton.addEventListener("click", () => {
    useCustomImages = !useCustomImages;
    updateComputerInfo(laptopSelect.value - 1);
})

buyNowButton.addEventListener("click", (() => {
    let computer = computers[laptopSelect.value - 1];
    bank.balance -= computer.price;
    updateDisplays();
    alert("Congratulations on purchasing your brand new " + computer.title);
}));

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(comnputers => processComputersData(computers))
    .then(() => updateComputerInfo(0));

console.log("script loaded");