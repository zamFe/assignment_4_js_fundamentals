class Work {
    constructor() {
        this.pay = 0
        this.income = 100;
    }
    doWork() {
        this.pay += this.income;
    }
    bankPay(bank) {
        if (bank.hasLoan) {
            let loanSub = (this.pay / 100) * 10;
            bank.payLoan(loanSub)
            bank.deposit(this.pay - loanSub);
            this.pay = 0;
        } else {
            bank.deposit(this.pay);
            this.pay = 0;
        }
    }
}