class Bank {
    constructor() {
        this.balance = 0;
        this.loan = 0;
        this.hasLoan = false;
    }
    deposit(amount) {
        this.balance += amount;
    }
    getLoan(amount) {
        amount = parseInt(amount);
        if (isNaN(amount)) {
            alert("Invalid input");
            return;
        }
        if (this.hasLoan) {
            alert("you still have an active loan")
            return;
        } else if (this.balance * 2 < amount) {
            alert("cannot take a loan greater than twice of your balance")
            return;
        }
        this.loan = amount;
        this.balance += amount;
        this.hasLoan = true;
        return;
    }
    payLoan(amount) {
        if (amount >= this.loan) {
            this.balance += (amount - this.loan);
            this.loan = 0;
            this.hasLoan = false;
        } else {
            this.loan -= amount;
        }
    }
}